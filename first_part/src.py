from collections import defaultdict
import re

# Task 1: Find the most recurrent sequence of characters in a list of keywords
def most_recurrent_sequence(keywords):
    sequence_count = defaultdict(int)
    
    for keyword in keywords:
        length = len(keyword)
        for i in range(length):
            for j in range(i + 1, length + 1):
                sequence = keyword[i:j]
                sequence_count[sequence] += 1
    
    most_recurrent = max(sequence_count, key=sequence_count.get)
    return most_recurrent

# Task 2: Parse nutritional values from a text and return a dictionary
def parse_nutritional_values(text):
    nutrition_dict = {}
    pattern = r'([a-zA-Zéèàêâôîûç\s]+) : ([\d,.]+ ?[a-zA-Z%]*)'
    
    matches = re.findall(pattern, text)
    for match in matches:
        name, value = match
        nutrition_dict[name.strip()] = value.strip()
    
    return nutrition_dict

# Task 3: Check if a list is a grandma list
def is_grandma_list(lst):
    def check_adjacent_product(sublist):
        if isinstance(sublist, list):
            for i in range(len(sublist)):
                if isinstance(sublist[i], list):
                    if check_adjacent_product(sublist[i]):
                        return True
                else:
                    for j in range(i + 1, len(sublist)):
                        product = sublist[i] * sublist[j]
                        if product in sublist:
                            return True
        return False

    return check_adjacent_product(lst)