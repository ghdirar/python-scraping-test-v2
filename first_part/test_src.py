import unittest
from src import most_recurrent_sequence, parse_nutritional_values, is_grandma_list

class TestSrc(unittest.TestCase):

    # Test for Task 1
    def test_most_recurrent_sequence(self):
        keywords = ['milk', 'chocolate', 'c+', 'python', 'cat', 'dog']
        result = most_recurrent_sequence(keywords)
        self.assertIn(result, ['at', 'ho'])

    # Test for Task 2
    def test_parse_nutritional_values(self):
        text = "Additifs nutritionnels : Vitamine C-D3 : 160 UI, Fer (3b103) : 4mg, Iode (3b202) : 0,28 mg, Cuivre (3b405, 3b406) : 2,2 mg,Manganèse (3b502, 3b503, 3b504) : 1,1 mg, Zinc (3b603,3b605, 3b606) : 11 mg –Clinoptilolited’origine sédimentaire : 2 g. Protéine : 11,0 % - Teneur en matières grasses : 4,5 % - Cendres brutes : 1,7 % - Cellulose brute : 0,5 % - Humidité : 80,0 %."
        expected_output = {
            "Vitamine C-D3": "160 UI", 
            "Fer (3b103)": "4mg", 
            "Iode (3b202)": "0,28 mg", 
            "Cuivre (3b405, 3b406)": "2,2 mg",
            "Manganèse (3b502, 3b503, 3b504)": "1,1 mg", 
            "Zinc (3b603,3b605, 3b606)": "11 mg", 
            "Clinoptilolited’origine sédimentaire": "2 g", 
            "Protéine": "11,0 %", 
            "Teneur en matières grasses": "4,5 %", 
            "Cendres brutes": "1,7 %", 
            "Cellulose brute": "0,5 %", 
            "Humidité": "80,0 %"
        }
        result = parse_nutritional_values(text)
        self.assertEqual(result, expected_output)

    # Test for Task 3
    def test_is_grandma_list(self):
        lst1 = [1, 2, [[4, 5], [4, 7]], 5, 4, [[95], [2]]]
        lst2 = [5, 9, 4, [[8, 7]], 4, 7, [[5]]]
        self.assertTrue(is_grandma_list(lst1))
        self.assertFalse(is_grandma_list(lst2))

if __name__ == '__main__':
    # unittest.main()